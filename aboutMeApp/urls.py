from django.urls import path
from .views import aboutMe

app_name = "aboutMeApp"

urlpatterns = [
	path('', aboutMe, name="aboutMe")
]
