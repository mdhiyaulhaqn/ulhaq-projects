from django.shortcuts import render
from django.http import HttpResponse
from .models import myModels

# Create your views here.
def aboutMe(request):
    allModels = myModels.objects.all()
    response = {'result': allModels}
    html = 'aboutMe.html' 
    return render(request, html, response)
